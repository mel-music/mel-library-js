# Mel Library Documentation

[API Documentation](./api.md)  
[File system interface](./file-system.md)  
[Data structures](./data-structures.md)
