# Data structures

## Artist

```json
{
  "id": "String",
  "name": "String",
  "releaseRelations": [
    {
      "type": "String",
      "release": "Release"
    }
  ]
}
```

- **id**: GMID of the artist
- **name**: Name of the artist
- **releaseRelations**: Releases that have a relation to the artist, types of relation:
  - `release`: Releases that the artist published
  - `guest`: Releases the artist has a guest appearance on

## Release

```json
{
  "id": "String",
  "title": "String",
  "year": "Number",
  "tracks": "Array<Track>",
  "artistRelations": [
    {
      "type": "String",
      "artist": "Artist"
    }
  ]
}
```

- **id**: GMID of the release
- **title**: Title of the release
- **year**: The year the release was published
- **tracks**: Tracks that were released on this release
- **artistRelations**: Artists that have a relation to this release, types:
  - `release`: Artists that published this release
  - `guest`: Artists that have a guest apperance on this release

## Track

```json
{
  "id": "String",
  "title": "String",
  "number": "Number",
  "discNumber": "Number",
  "release": "Release",
  "artistRelations": [
    {
      "type": "String",
      "artist": "Artist"
    }
  ]
}
```

- **id**: GMID of the track
- **title**: Title of the track
- **number**: Number of the track in the respective release
- **discNumber**: Number of the disc the track is part of on the release
- **release**: The release the track was released on
- **artistRelations**: List of artists that have a relation to this track, type describes the kind of releation:
  - `artist`: Artists that are performing artists on the track

## File

```json
{
  "path": "String",
  "type": "String",
  "buffer": "ArrayBuffer",
  "stats": {},
  "track": "Track"
}
```

- **path**: Location of the file in the local file system
- **type**: Type of the audio file: "flac", "mp3"
- **buffer**: Buffer containing the audio data
- **stats**: Various file stats, not specified yet
- **track**: The track that the file represents
