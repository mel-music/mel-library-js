const File = require("../../src/files/file");
const { MP3, FLAC } = File;

test("constructor parameter retrievable by getters", () => {
  const PATH = "/test/path";
  const TYPE = MP3;
  const BUFFER = new ArrayBuffer();
  const STATS = {};
  const TRACK = {};
  const file = new File(PATH, TYPE, BUFFER, STATS, TRACK);

  expect(file).toHaveProperty("getPath");
  expect(file.getPath).toBeInstanceOf(Function);
  expect(file.getPath()).toBe(PATH);

  expect(file).toHaveProperty("getType");
  expect(file.getType).toBeInstanceOf(Function);
  expect(file.getType()).toBe(TYPE);

  expect(file).toHaveProperty("getBuffer");
  expect(file.getBuffer).toBeInstanceOf(Function);
  expect(file.getBuffer()).toBe(BUFFER);

  expect(file).toHaveProperty("getStats");
  expect(file.getStats).toBeInstanceOf(Function);
  expect(file.getStats()).toBe(STATS);

  expect(file).toHaveProperty("getTrack");
  expect(file.getTrack).toBeInstanceOf(Function);
  expect(file.getTrack()).toBe(TRACK);
});

test("using setters changes getter values", () => {
  const PATH = "/test/path";
  const TYPE = MP3;
  const BUFFER = new ArrayBuffer();
  const STATS = {};
  const TRACK = {};
  const file = new File(null, null, null, null, null);

  expect(file.getPath()).not.toBe(PATH);
  file.setPath(PATH);
  expect(file.getPath()).toBe(PATH);

  expect(file.getType()).not.toBe(TYPE);
  file.setType(TYPE);
  expect(file.getType()).toBe(TYPE);

  expect(file.getBuffer()).not.toBe(BUFFER);
  file.setBuffer(BUFFER);
  expect(file.getBuffer()).toBe(BUFFER);

  expect(file.getStats()).not.toBe(STATS);
  file.setStats(STATS);
  expect(file.getStats()).toBe(STATS);

  expect(file.getTrack()).not.toBe(TRACK);
  file.setTrack(TRACK);
  expect(file.getTrack()).toBe(TRACK);
});

test("deleting buffer works", () => {
  const PATH = "/test/path";
  const TYPE = MP3;
  const BUFFER = new ArrayBuffer();
  const STATS = {};
  const TRACK = {};
  const file = new File(PATH, TYPE, BUFFER, STATS, TRACK);
  
  expect(file.getBuffer()).toBe(BUFFER);
  file.deleteBuffer();
  expect(file.getBuffer()).not.toBe(BUFFER);
});
