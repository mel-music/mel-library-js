const Artist = require("../../src/media/artist");
const Release = require("../../src/media/release");
const Constants = require("../../src/core/constants");

const { RELEASE_ARTIST, RELEASE_GUEST } = Constants.relationTypes;

test("data passed to constructor is retrievable by getters", () => {
  const ARTIST_ID = "artist_id";
  const ARTIST_NAME = "artist_name";
  const RELEASE_RELATIONS = [
    { type: RELEASE_ARTIST, release: new Release("release_a") },
  ];
  const artist = new Artist(ARTIST_ID, ARTIST_NAME, RELEASE_RELATIONS);

  expect(artist).toHaveProperty("getId");
  expect(artist.getId).toBeInstanceOf(Function);
  expect(artist.getId()).toBe(ARTIST_ID);

  expect(artist).toHaveProperty("getName");
  expect(artist.getName).toBeInstanceOf(Function);
  expect(artist.getName()).toBe(ARTIST_NAME);

  expect(artist).toHaveProperty("getReleaseRelations");
  expect(artist.getReleaseRelations).toBeInstanceOf(Function);
  expect(artist.getReleaseRelations()).toEqual(
    expect.arrayContaining(RELEASE_RELATIONS)
  );
});

test("using setters changes values returned by getters accordingly", () => {
  const ARTIST_ID = "artist_id";
  const ARTIST_NAME = "artist_name";
  const RELEASE_RELATIONS = [
    { type: RELEASE_ARTIST, release: new Release("release_a") },
  ];
  const artist = new Artist();

  expect(artist.getId()).not.toBe(ARTIST_ID);
  artist.setId(ARTIST_ID);
  expect(artist.getId()).toBe(ARTIST_ID);

  expect(artist.getName()).not.toBe(ARTIST_NAME);
  artist.setName(ARTIST_NAME);
  expect(artist.getName()).toBe(ARTIST_NAME);

  expect(artist.getReleaseRelations()).not.toEqual(
    expect.arrayContaining(RELEASE_RELATIONS)
  );
  artist.setReleaseRelations(RELEASE_RELATIONS);
  expect(artist.getReleaseRelations()).toEqual(
    expect.arrayContaining(RELEASE_RELATIONS)
  );
});

test("toString() returns data as string", () => {
  const ARTIST_ID = "artist_id";
  const ARTIST_NAME = "artist_name";
  const RELEASE_RELATIONS = [
    { type: RELEASE_ARTIST, release: new Release("", "release") },
  ];
  const artist = new Artist(ARTIST_ID, ARTIST_NAME, RELEASE_RELATIONS);

  expect(artist.toString()).toEqual(
    "Artist {name: artist_name, releases: release, id: artist_id}"
  );
});

test("getReleases returns releases based on relation type", () => {
  const RELEASE = new Release("release_a");
  const FEATURE_RELEASE = new Release("release_b");

  const RELEASE_RELATIONS = [
    { type: RELEASE_ARTIST, release: RELEASE },
    { type: RELEASE_GUEST, release: FEATURE_RELEASE },
  ];

  let artist = new Artist("id", "name", RELEASE_RELATIONS);

  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE);
  expect(artist.getReleases(RELEASE_ARTIST)).not.toContain(FEATURE_RELEASE);
  expect(artist.getReleases(RELEASE_GUEST)).toContain(FEATURE_RELEASE);
  expect(artist.getReleases(RELEASE_GUEST)).not.toContain(RELEASE);
});

test("addRelease adds a release to artist, artist of release is changed accordingly", () => {
  const RELEASE = new Release();

  let artist = new Artist();

  expect(RELEASE.getArtists(RELEASE_ARTIST)).not.toContain(artist);
  expect(artist.getReleases(RELEASE_ARTIST)).not.toContain(RELEASE);
  artist.addRelease(RELEASE, RELEASE_ARTIST);
  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE);
  expect(RELEASE.getArtists(RELEASE_ARTIST)).toContain(artist);
});

test("addReleases adds multiple releases to artist, all releases artists get changed accordingly", () => {
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");

  let artist = new Artist();

  expect(RELEASE_A.getArtists(RELEASE_ARTIST)).not.toContain(artist);
  expect(RELEASE_B.getArtists(RELEASE_ARTIST)).not.toContain(artist);
  expect(artist.getReleases(RELEASE_ARTIST)).not.toContain(RELEASE_A);
  expect(artist.getReleases(RELEASE_ARTIST)).not.toContain(RELEASE_B);
  artist.addReleases([RELEASE_A, RELEASE_B], RELEASE_ARTIST);
  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE_A);
  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE_B);
  expect(RELEASE_A.getArtists(RELEASE_ARTIST)).toContain(artist);
  expect(RELEASE_B.getArtists(RELEASE_ARTIST)).toContain(artist);
});

test("complement adds missing data from provided artist", () => {
  const RELEASE = new Release("release");
  const FEATURE_RELEASE = new Release("feature");
  const RELEASE_RELATIONS = [
    { type: RELEASE_ARTIST, release: RELEASE },
    { type: RELEASE_GUEST, release: FEATURE_RELEASE },
  ];
  const ID = "id";
  const NAME = "name";
  let artist = new Artist();
  let complementingArtist = new Artist(ID, NAME, RELEASE_RELATIONS);

  expect(artist.getId()).not.toEqual(ID);
  expect(artist.getName()).not.toEqual(NAME);
  expect(artist.getReleases(RELEASE_ARTIST)).not.toContain(RELEASE);
  expect(artist.getReleases(RELEASE_GUEST)).not.toContain(FEATURE_RELEASE);
  artist = artist.complement(complementingArtist);
  expect(artist.getId()).toEqual(ID);
  expect(artist.getName()).toEqual(NAME);
  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE);
  expect(artist.getReleases(RELEASE_GUEST)).toContain(FEATURE_RELEASE);
});

test("complement doesnt overwrite existing data", () => {
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");
  const RELEASE_RELATIONS_A = [
    { type: RELEASE_ARTIST, release: RELEASE_A },
    { type: RELEASE_GUEST, release: RELEASE_B },
  ];
  const RELEASE_RELATIONS_B = [
    { type: RELEASE_ARTIST, release: RELEASE_B },
    { type: RELEASE_GUEST, release: RELEASE_A },
  ];
  const ID_A = "ida";
  const ID_B = "idb";
  const NAME_A = "namea";
  const NAME_B = "nameb";
  let artist = new Artist(ID_A, NAME_A, RELEASE_RELATIONS_A);
  let complementingArtist = new Artist(ID_B, NAME_B, RELEASE_RELATIONS_B);

  expect(artist.getId()).toEqual(ID_A);
  expect(artist.getName()).toEqual(NAME_A);
  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE_A);
  expect(artist.getReleases(RELEASE_ARTIST)).not.toContain(RELEASE_B);
  expect(artist.getReleases(RELEASE_GUEST)).toContain(RELEASE_B);
  expect(artist.getReleases(RELEASE_GUEST)).not.toContain(RELEASE_A);
  artist = artist.complement(complementingArtist);
  expect(artist.getId()).toEqual(ID_A);
  expect(artist.getName()).toEqual(NAME_A);
  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE_A);
  expect(artist.getReleases(RELEASE_ARTIST)).toContain(RELEASE_B);
  expect(artist.getReleases(RELEASE_GUEST)).toContain(RELEASE_B);
  expect(artist.getReleases(RELEASE_GUEST)).toContain(RELEASE_A);
});
