const Track = require("../../src/media/track");
const Artist = require("../../src/media/artist");
const Release = require("../../src/media/release");
const Constants = require("../../src/core/constants");

const { TRACK_ARTIST } = Constants.relationTypes;

test("data passed to constructor is retrievable by getters", () => {
  const ID = "id";
  const TITLE = "track";
  const NUMBER = 3;
  const DISC_NUMBER = 2;
  const RELEASE = new Release();
  const ARTIST_RELATIONS = [
    { type: TRACK_ARTIST, artist: new Artist("artist") },
  ];

  let track = new Track(
    ID,
    TITLE,
    NUMBER,
    DISC_NUMBER,
    RELEASE,
    ARTIST_RELATIONS
  );

  expect(track).toHaveProperty("getId");
  expect(track.getId).toBeInstanceOf(Function);
  expect(track.getId()).toBe(ID);

  expect(track).toHaveProperty("getTitle");
  expect(track.getTitle).toBeInstanceOf(Function);
  expect(track.getTitle()).toBe(TITLE);

  expect(track).toHaveProperty("getNumber");
  expect(track.getNumber).toBeInstanceOf(Function);
  expect(track.getNumber()).toBe(NUMBER);

  expect(track).toHaveProperty("getDiscNumber");
  expect(track.getDiscNumber).toBeInstanceOf(Function);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER);

  expect(track).toHaveProperty("getRelease");
  expect(track.getRelease).toBeInstanceOf(Function);
  expect(track.getRelease()).toBe(RELEASE);

  expect(track).toHaveProperty("getArtistRelations");
  expect(track.getArtistRelations).toBeInstanceOf(Function);
  expect(track.getArtistRelations()).toBe(ARTIST_RELATIONS);
});

test("using setters changes values returned by getters accordingly", () => {
  const ID = "id";
  const TITLE = "track";
  const NUMBER = 3;
  const DISC_NUMBER = 2;
  const RELEASE = new Release();
  const ARTIST_RELATIONS = [{ type: TRACK_ARTIST, artist: new Artist() }];

  let track = new Track();

  expect(track.getId()).not.toBe(ID);
  track.setId(ID);
  expect(track.getId()).toBe(ID);

  expect(track.getTitle()).not.toBe(TITLE);
  track.setTitle(TITLE);
  expect(track.getTitle()).toBe(TITLE);

  expect(track.getNumber()).not.toBe(NUMBER);
  track.setNumber(NUMBER);
  expect(track.getNumber()).toBe(NUMBER);

  expect(track.getDiscNumber()).not.toBe(DISC_NUMBER);
  track.setDiscNumber(DISC_NUMBER);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER);

  expect(track.getRelease()).not.toBe(RELEASE);
  track.setRelease(RELEASE);
  expect(track.getRelease()).toBe(RELEASE);

  expect(track.getArtistRelations()).not.toBe(ARTIST_RELATIONS);
  track.setArtistRelations(ARTIST_RELATIONS);
  expect(track.getArtistRelations()).toBe(ARTIST_RELATIONS);
});

test("toString() returns data as string", () => {
  const ID = "id";
  const TITLE = "track";
  const NUMBER = 3;
  const DISC_NUMBER = 2;
  const RELEASE = new Release();
  const ARTIST_RELATIONS = [{ type: TRACK_ARTIST, artist: new Artist() }];

  let track = new Track(
    ID,
    TITLE,
    NUMBER,
    DISC_NUMBER,
    RELEASE,
    ARTIST_RELATIONS
  );

  expect(track.toString()).toEqual(
    "Track {title: track, artists: , release: undefined, number: 3, discNumber: 2, id: id}"
  );
});

test("addArtist adds an artist to the track", () => {
  const ID = "id";
  const TITLE = "track";
  const NUMBER = 3;
  const DISC_NUMBER = 2;
  const RELEASE = new Release();
  const ARTIST = new Artist("A");
  const ARTIST_RELATIONS = [{ type: TRACK_ARTIST, artist: new Artist("B") }];

  let track = new Track(
    ID,
    TITLE,
    NUMBER,
    DISC_NUMBER,
    RELEASE,
    ARTIST_RELATIONS
  );

  expect(track.getArtists(TRACK_ARTIST)).not.toContain(ARTIST);
  track.addArtist(ARTIST, TRACK_ARTIST);
  expect(track.getArtists(TRACK_ARTIST)).toContain(ARTIST);
});

test("addArtists adds multiple artists to the track", () => {
  const ID = "id";
  const TITLE = "track";
  const NUMBER = 3;
  const DISC_NUMBER = 2;
  const RELEASE = new Release();
  const NEW_ARTISTS = [new Artist("A"), new Artist("B")];
  const ARTIST_RELATIONS = [{ type: TRACK_ARTIST, artist: new Artist("C") }];

  let track = new Track(
    ID,
    TITLE,
    NUMBER,
    DISC_NUMBER,
    RELEASE,
    ARTIST_RELATIONS
  );

  expect(track.getArtists(TRACK_ARTIST)).not.toEqual(
    expect.arrayContaining(NEW_ARTISTS)
  );
  track.addArtists(NEW_ARTISTS, TRACK_ARTIST);
  expect(track.getArtists(TRACK_ARTIST)).toEqual(
    expect.arrayContaining(NEW_ARTISTS)
  );
});

test("getArtists returns artists of specific relation type", () => {
  const ID = "id";
  const TITLE = "track";
  const NUMBER = 3;
  const DISC_NUMBER = 2;
  const RELEASE = new Release();
  const ARTIST_A = new Artist("artist_a");
  const ARTIST_B = new Artist("artist_b");
  const ARTIST_RELATIONS = [
    { type: TRACK_ARTIST, artist: ARTIST_A },
    { type: "random_type", artist: ARTIST_B },
  ];

  let track = new Track(
    ID,
    TITLE,
    NUMBER,
    DISC_NUMBER,
    RELEASE,
    ARTIST_RELATIONS
  );

  expect(track.getArtists(TRACK_ARTIST)).toContain(ARTIST_A);
  expect(track.getArtists(TRACK_ARTIST)).not.toContain(ARTIST_B);
});

test("complement adds missing data from provided track", () => {
  const ID = "id";
  const TITLE = "track";
  const NUMBER = 3;
  const DISC_NUMBER = 2;
  const RELEASE = new Release();
  const ARTIST_A = new Artist("artist_a");
  const ARTIST_B = new Artist("artist_b");
  const ARTIST_RELATIONS = [
    { type: TRACK_ARTIST, artist: ARTIST_A },
    { type: TRACK_ARTIST, artist: ARTIST_B },
  ];

  let track = new Track();
  let complementingTrack = new Track(
    ID,
    TITLE,
    NUMBER,
    DISC_NUMBER,
    RELEASE,
    ARTIST_RELATIONS
  );

  expect(track.getId()).not.toBe(ID);
  expect(track.getTitle()).not.toBe(TITLE);
  expect(track.getArtists(TRACK_ARTIST)).not.toContain(ARTIST_A);
  expect(track.getArtists(TRACK_ARTIST)).not.toContain(ARTIST_B);
  expect(track.getRelease()).not.toBe(RELEASE);
  expect(track.getNumber()).not.toBe(NUMBER);
  expect(track.getDiscNumber()).not.toBe(DISC_NUMBER);
  track = track.complement(complementingTrack);
  expect(track.getId()).toBe(ID);
  expect(track.getTitle()).toBe(TITLE);
  expect(track.getArtists(TRACK_ARTIST)).toContain(ARTIST_A);
  expect(track.getArtists(TRACK_ARTIST)).toContain(ARTIST_B);
  expect(track.getRelease()).toEqual(RELEASE);
  expect(track.getNumber()).toBe(NUMBER);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER);
});

test("complement doesnt overwrite existing data", () => {
  const ID_A = "idA";
  const ID_B = "idB";
  const TITLE_A = "trackA";
  const TITLE_B = "trackB";
  const NUMBER_A = 3;
  const NUMBER_B = 5;
  const DISC_NUMBER_A = 2;
  const DISC_NUMBER_B = 1;
  const RELEASE_A = new Release("A");
  const RELEASE_B = new Release("B");
  const ARTIST_A = new Artist("A");
  const ARTIST_B = new Artist("B");
  const ARTIST_RELATIONS_A = [{ type: TRACK_ARTIST, artist: ARTIST_A }];
  const ARTIST_RELATIONS_B = [{ type: TRACK_ARTIST, artist: ARTIST_B }];

  let track = new Track(
    ID_A,
    TITLE_A,
    NUMBER_A,
    DISC_NUMBER_A,
    RELEASE_A,
    ARTIST_RELATIONS_A
  );
  let complementingTrack = new Track(
    ID_B,
    TITLE_B,
    NUMBER_B,
    DISC_NUMBER_B,
    RELEASE_B,
    ARTIST_RELATIONS_B
  );

  expect(track.getId()).toBe(ID_A);
  expect(track.getTitle()).toBe(TITLE_A);
  expect(track.getArtists(TRACK_ARTIST)).toContain(ARTIST_A);
  expect(track.getArtists(TRACK_ARTIST)).not.toContain(ARTIST_B);
  expect(track.getRelease()).toBe(RELEASE_A);
  expect(track.getNumber()).toBe(NUMBER_A);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER_A);
  track = track.complement(complementingTrack);
  expect(track.getId()).toBe(ID_A);
  expect(track.getTitle()).toBe(TITLE_A);
  expect(track.getArtists(TRACK_ARTIST)).toContain(ARTIST_A);
  expect(track.getArtists(TRACK_ARTIST)).toContain(ARTIST_B);
  expect(track.getRelease()).toBe(RELEASE_A);
  expect(track.getNumber()).toBe(NUMBER_A);
  expect(track.getDiscNumber()).toBe(DISC_NUMBER_A);
});
