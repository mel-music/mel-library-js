const Artist = require("../../src/media/artist");
const Release = require("../../src/media/release");
const Track = require("../../src/media/track");
const Serializer = require("../../src/utils/serializer");

const { ARTIST_TYPE } = Track;
const { RELEASE_TYPE } = Release;

test("serializeArtist serializes an artist to an object", () => {
  const artist = new Artist("id", "artist_name", [
    { type: RELEASE_TYPE, release: new Release("release_a") },
    { type: RELEASE_TYPE, release: new Release("release_b") },
  ]);

  const artistObject = Serializer.serializeArtist(artist);

  checkArtistObject(
    artistObject,
    artist.getId(),
    artist.getName(),
    artist.getReleaseRelations().map((relation) => relation.release.getId())
  );
});

test("deserializeArtist deserializes an object to an artist", () => {
  const artistObject = {
    id: "id",
    name: "artist_name",
    release_relations: [
      { type: RELEASE_TYPE, release: { id: "release_a" } },
      { type: RELEASE_TYPE, release: { id: "release_b" } },
    ],
  };

  const artist = Serializer.deserializeArtist(artistObject);

  checkArtist(
    artist,
    artistObject.id,
    artistObject.name,
    artistObject.release_relations.map((relation) => relation.release.id)
  );
});

test("serializeArtists serializes an array of artists to an array of objects", () => {
  const artists = [
    new Artist("id_a", "artist_a", [
      { type: RELEASE_TYPE, release: new Release("release_a") },
      { type: RELEASE_TYPE, release: new Release("release_b") },
    ]),
    new Artist("id_b", "artist_b", [
      { type: RELEASE_TYPE, release: new Release("release_c") },
      { type: RELEASE_TYPE, release: new Release("release_d") },
    ]),
  ];

  const artistObjects = Serializer.serializeArtists(artists);

  artists.forEach((artist, index) =>
    checkArtistObject(
      artistObjects[index],
      artist.getId(),
      artist.getName(),
      artist.getReleaseRelations().map((relation) => relation.release.getId())
    )
  );
});

test("deserializeArtists deserializes an array of objects to an array of artists", () => {
  const artistObjects = [
    {
      id: "artist_a",
      name: "artist_name_a",
      release_relations: [
        { type: RELEASE_TYPE, release: { id: "release_a" } },
        { type: RELEASE_TYPE, release: { id: "release_b" } },
      ],
    },
    {
      id: "artist_b",
      name: "artist_name_b",
      release_relations: [
        { type: RELEASE_TYPE, release: { id: "release_c" } },
        { type: RELEASE_TYPE, release: { id: "release_d" } },
      ],
    },
  ];

  const artists = Serializer.deserializeArtists(artistObjects);

  artistObjects.forEach((artistObject, index) =>
    checkArtist(
      artists[index],
      artistObject.id,
      artistObject.name,
      artistObject.release_relations.map((relation) => relation.release.id)
    )
  );
});

test("serializeRelease serializes a release to an object", () => {
  const release = new Release(
    "id",
    "title",
    2021,
    [new Track("track_a"), new Track("track_b")],
    [
      { type: RELEASE_TYPE, artist: new Artist("artist_a") },
      { type: RELEASE_TYPE, artist: new Artist("artist_b") },
    ]
  );

  const releaseObject = Serializer.serializeRelease(release);

  checkReleaseObject(
    releaseObject,
    release.getId(),
    release.getArtistRelations().map((relation) => relation.artist.getId()),
    release.getTitle(),
    release.getYear(),
    release.getTracks().map((track) => track.getId())
  );
});

test("deserializeRelease deserializes an object to a release", () => {
  const releaseObject = {
    id: "id",
    artist: { id: "artist_id" },
    title: "title",
    year: 2021,
    tracks: [{ id: "track_a" }, { id: "track_b" }],
    feature_artists: [{ id: "feature_artist_a" }, { id: "feature_artist_b" }],
  };

  const release = Serializer.deserializeRelease(releaseObject);

  checkRelease(
    release,
    releaseObject.id,
    releaseObject.artist.id,
    releaseObject.title,
    releaseObject.year,
    releaseObject.tracks.map((track) => track.id),
    releaseObject.feature_artists.map((artist) => artist.id)
  );
});

test("serializeReleases serializes an array of releases to an array of objects", () => {
  const releases = [
    new Release(
      "release_a",
      "title_a",
      2020,
      [new Track("track_a"), new Track("track_b")],
      [
        { type: RELEASE_TYPE, artist: new Artist("artist_a") },
        { type: RELEASE_TYPE, artist: new Artist("artist_b") },
      ]
    ),
    new Release(
      "release_b",
      "title_b",
      2021,
      [new Track("track_c"), new Track("track_d")],
      [
        { type: RELEASE_TYPE, artist: new Artist("artist_c") },
        { type: RELEASE_TYPE, artist: new Artist("artist_d") },
      ]
    ),
  ];

  const releaseObjects = Serializer.serializeReleases(releases);

  releases.forEach((release, index) =>
    checkReleaseObject(
      releaseObjects[index],
      release.getId(),
      release.getArtists().map((relation) => relation.artist.getId()),
      release.getTitle(),
      release.getYear(),
      release.getTracks().map((track) => track.getId())
    )
  );
});

test("deserializeReleases deserializes an array of objects to an array of releases", () => {
  const releaseObjects = [
    {
      id: "release_a",
      title: "title_a",
      year: 2020,
      tracks: [{ id: "track_a" }, { id: "track_b" }],
      artists: [
        { type: RELEASE_TYPE, artist: { id: "artist_a" } },
        { type: RELEASE_TYPE, artist: { id: "artist_b" } },
      ],
    },
    {
      id: "release_b",
      title: "title_b",
      year: 2021,
      tracks: [{ id: "track_c" }, { id: "track_d" }],
      artists: [
        { type: RELEASE_TYPE, artist: { id: "artist_c" } },
        { type: RELEASE_TYPE, artist: { id: "artist_d" } },
      ],
    },
  ];

  const releases = Serializer.deserializeReleases(releaseObjects);

  releaseObjects.forEach((releaseObject, index) =>
    checkRelease(
      releases[index],
      releaseObject.id,
      releaseObject.artists.map((relation) => relation.artist.id),
      releaseObject.title,
      releaseObject.year,
      releaseObject.tracks.map((track) => track.id)
    )
  );
});

test("serializeTrack serializes a track to an object", () => {
  const track = new Track("id", "title", 1, 2, new Release("release_id"), [
    { type: ARTIST_TYPE, artist: new Artist("artist_a") },
    { type: ARTIST_TYPE, artist: new Artist("artist_b") },
  ]);

  const trackObject = Serializer.serializeTrack(track);

  checkTrackObject(
    trackObject,
    track.getId(),
    track.getTitle(),
    track.getArtistRelations().map((relation) => relation.artist.getId()),
    track.getRelease().getId(),
    track.getNumber(),
    track.getDiscNumber()
  );
});

test("deserializeTrack deserializes an object to a track", () => {
  const trackObject = {
    id: "id",
    title: "title",
    number: 1,
    disc_number: 2,
    release: { id: "release_id" },
    artist_relations: [
      { type: ARTIST_TYPE, artist: { id: "artist_a" } },
      { type: ARTIST_TYPE, artist: { id: "artist_b" } },
    ],
  };

  const track = Serializer.deserializeTrack(trackObject);

  checkTrack(
    track,
    trackObject.id,
    trackObject.title,
    trackObject.artist_relations.map((relation) => relation.artist.id),
    trackObject.release.id,
    trackObject.number,
    trackObject.disc_number
  );
});

test("serializeTracks serializes an array of tracks to an array of objects", () => {
  const tracks = [
    new Track("track_a", "title_a", 1, 2, new Release("release_a"), [
      { type: ARTIST_TYPE, artist: new Artist("artist_a") },
      { type: ARTIST_TYPE, artist: new Artist("artist_b") },
    ]),
    new Track("track_b", "title_b", 2, 1, new Release("release_b"), [
      { type: ARTIST_TYPE, artist: new Artist("artist_c") },
      { type: ARTIST_TYPE, artist: new Artist("artist_d") },
    ]),
  ];

  const trackObjects = Serializer.serializeTracks(tracks);

  tracks.forEach((track, index) =>
    checkTrackObject(
      trackObjects[index],
      track.getId(),
      track.getTitle(),
      track.getArtistRelations().map((relation) => relation.artist.getId()),
      track.getRelease().getId(),
      track.getNumber(),
      track.getDiscNumber()
    )
  );
});

test("deserializeTracks deserializes an array objects to an array of tracks", () => {
  const trackObjects = [
    {
      id: "track_a",
      title: "title_a",
      number: 1,
      disc_number: 2,
      release: { id: "release_a" },
      artist_relations: [
        { type: ARTIST_TYPE, artist: { id: "artist_a" } },
        { type: ARTIST_TYPE, artist: { id: "artist_b" } },
      ],
    },
    {
      id: "track_b",
      title: "title_b",
      number: 2,
      disc_number: 1,
      release: { id: "release_b" },
      artist_relations: [
        { type: ARTIST_TYPE, artist: { id: "artist_c" } },
        { type: ARTIST_TYPE, artist: { id: "artist_d" } },
      ],
    },
  ];

  const tracks = Serializer.deserializeTracks(trackObjects);

  trackObjects.forEach((trackObject, index) =>
    checkTrack(
      tracks[index],
      trackObject.id,
      trackObject.title,
      trackObject.artist_relations.map((relation) => relation.artist.id),
      trackObject.release.id,
      trackObject.number,
      trackObject.disc_number
    )
  );
});

function isArtist(artist) {
  expect(artist).toHaveProperty("addRelease");
  expect(artist).toHaveProperty("addReleases");
  expect(artist).toHaveProperty("complement");
  expect(artist).toHaveProperty("toString");
  expect(artist).toHaveProperty("equals");
  expect(artist).toHaveProperty("getName");
  expect(artist).toHaveProperty("setName");
  expect(artist).toHaveProperty("getReleases");
  expect(artist).toHaveProperty("getId");
  expect(artist).toHaveProperty("setId");
  expect(artist).toHaveProperty("getReleaseRelations");
  expect(artist).toHaveProperty("setReleaseRelations");
}

function isRelease(release) {
  expect(release).toHaveProperty("addTrack");
  expect(release).toHaveProperty("addTracks");
  expect(release).toHaveProperty("deleteTrack");
  expect(release).toHaveProperty("complement");
  expect(release).toHaveProperty("toString");
  expect(release).toHaveProperty("equals");
  expect(release).toHaveProperty("getArtists");
  expect(release).toHaveProperty("getTitle");
  expect(release).toHaveProperty("setTitle");
  expect(release).toHaveProperty("getYear");
  expect(release).toHaveProperty("setYear");
  expect(release).toHaveProperty("getTracks");
  expect(release).toHaveProperty("setTracks");
  expect(release).toHaveProperty("getId");
  expect(release).toHaveProperty("setId");
  expect(release).toHaveProperty("getArtistRelations");
  expect(release).toHaveProperty("setArtistRelations");
}

function isTrack(track) {
  expect(track).toHaveProperty("addArtist");
  expect(track).toHaveProperty("addArtists");
  expect(track).toHaveProperty("complement");
  expect(track).toHaveProperty("toString");
  expect(track).toHaveProperty("equals");
  expect(track).toHaveProperty("getTitle");
  expect(track).toHaveProperty("setTitle");
  expect(track).toHaveProperty("getArtists");
  expect(track).toHaveProperty("getRelease");
  expect(track).toHaveProperty("setRelease");
  expect(track).toHaveProperty("getNumber");
  expect(track).toHaveProperty("setNumber");
  expect(track).toHaveProperty("getDiscNumber");
  expect(track).toHaveProperty("setDiscNumber");
  expect(track).toHaveProperty("getId");
  expect(track).toHaveProperty("setId");
  expect(track).toHaveProperty("getArtistRelations");
  expect(track).toHaveProperty("setArtistRelations");
}

function checkArtistObject(artistObject, id, name, releaseIds) {
  expect(artistObject).toHaveProperty("id");
  expect(artistObject.id).toEqual(id);
  expect(artistObject).toHaveProperty("name");
  expect(artistObject.name).toEqual(name);
  expect(artistObject).toHaveProperty("release_relations");
  expect(artistObject.release_relations).toBeInstanceOf(Array);
  expect(artistObject.release_relations.length).toEqual(releaseIds.length);
  releaseIds.forEach((id, index) =>
    expect(artistObject.release_relations[index].release.id).toEqual(id)
  );
}

function checkArtist(artist, id, name, releaseIds) {
  isArtist(artist);
  expect(artist.getId()).toEqual(id);
  expect(artist.getName()).toEqual(name);
  expect(artist.getReleaseRelations()).toBeInstanceOf(Array);
  expect(artist.getReleaseRelations().length).toEqual(releaseIds.length);
  releaseIds.forEach((id, index) => {
    isRelease(artist.getReleaseRelations()[index].release);
    expect(artist.getReleaseRelations()[index].release.getId()).toEqual(id);
  });
}

function checkReleaseObject(
  releaseObject,
  id,
  artist_ids,
  title,
  year,
  track_ids
) {
  expect(releaseObject).toHaveProperty("id");
  expect(releaseObject.id).toBe(id);
  expect(releaseObject).toHaveProperty("artist_relations");
  artist_ids.forEach((artist_id, index) =>
    expect(releaseObject.artist_relations[index].artist.id).toBe(artist_id)
  );
  expect(releaseObject).toHaveProperty("title");
  expect(releaseObject.title).toBe(title);
  expect(releaseObject).toHaveProperty("year");
  expect(releaseObject.year).toBe(year);
  expect(releaseObject).toHaveProperty("tracks");
  expect(releaseObject.tracks).toBeInstanceOf(Array);
  expect(releaseObject.tracks.length).toBe(track_ids.length);
  track_ids.forEach((track_id, index) =>
    expect(releaseObject.tracks[index].id).toBe(track_id)
  );
}

function checkRelease(release, id, artist_ids, title, year, track_ids) {
  isRelease(release);
  expect(release.getId()).toBe(id);
  release
    .getArtists()
    .forEach((relation, index) =>
      expect(relation.artist.getId()).toEqual(artist_ids[index])
    );
  expect(release.getTitle()).toBe(title);
  expect(release.getYear()).toBe(year);
  expect(release.getTracks().length).toBe(track_ids.length);
  release
    .getTracks()
    .forEach((track, index) => expect(track.getId()).toBe(track_ids[index]));
}

function checkTrackObject(
  trackObject,
  id,
  title,
  artist_ids,
  release_id,
  number,
  discNumber
) {
  expect(trackObject).toHaveProperty("id");
  expect(trackObject.id).toBe(id);
  expect(trackObject).toHaveProperty("title");
  expect(trackObject.title).toBe(title);
  expect(trackObject).toHaveProperty("artist_relations");
  expect(trackObject.artist_relations).toBeInstanceOf(Array);
  expect(trackObject.artist_relations.length).toBe(artist_ids.length);
  artist_ids.forEach((artist_id, index) =>
    expect(trackObject.artist_relations[index].artist.id).toBe(artist_id)
  );
  expect(trackObject).toHaveProperty("release");
  expect(trackObject.release.id).toBe(release_id);
  expect(trackObject).toHaveProperty("number");
  expect(trackObject.number).toBe(number);
  expect(trackObject).toHaveProperty("disc_number");
  expect(trackObject.disc_number).toBe(discNumber);
}

function checkTrack(
  track,
  id,
  title,
  artist_ids,
  release_id,
  number,
  discNumber
) {
  isTrack(track);
  expect(track.getId()).toBe(id);
  expect(track.getTitle()).toBe(title);
  expect(track.getArtistRelations().length).toBe(artist_ids.length);
  track
    .getArtists()
    .forEach((relation, index) =>
      expect(relation.artist.getId()).toBe(artist_ids[index])
    );
  expect(track.getRelease().getId()).toBe(release_id);
  expect(track.getNumber()).toBe(number);
  expect(track.getDiscNumber()).toBe(discNumber);
}
