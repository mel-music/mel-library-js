const Release = require("../../src/media/release");
const Artist = require("../../src/media/artist");
const Track = require("../../src/media/track");
const Constants = require("../../src/core/constants");

const { RELEASE_ARTIST, RELEASE_GUEST } = Constants.relationTypes;

test("data passed to constructor is retrievable by getters", () => {
  const ID = "A";
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const ARTIST = new Artist("A");
  const FEATURE_ARTIST = new Artist("B");
  const ARTIST_RELATIONS = [
    { type: RELEASE_ARTIST, artist: ARTIST },
    { type: RELEASE_ARTIST, artist: FEATURE_ARTIST },
  ];

  let release = new Release(ID, TITLE, YEAR, TRACKS, ARTIST_RELATIONS);

  expect(release).toHaveProperty("getId");
  expect(release.getId).toBeInstanceOf(Function);
  expect(release.getId()).toBe(ID);

  expect(release).toHaveProperty("getTitle");
  expect(release.getTitle).toBeInstanceOf(Function);
  expect(release.getTitle()).toBe(TITLE);

  expect(release).toHaveProperty("getYear");
  expect(release.getYear).toBeInstanceOf(Function);
  expect(release.getYear()).toBe(YEAR);

  expect(release).toHaveProperty("getTracks");
  expect(release.getTracks).toBeInstanceOf(Function);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS));

  expect(release).toHaveProperty("getArtistRelations");
  expect(release.getArtistRelations).toBeInstanceOf(Function);
  expect(release.getArtistRelations()).toEqual(ARTIST_RELATIONS);
});

test("using setters changes values returned by getters accordingly", () => {
  const ID = "A";
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const ARTIST = new Artist("A");
  const FEATURE_ARTIST = new Artist("B");
  const ARTIST_RELATIONS = [
    { type: RELEASE_ARTIST, artist: ARTIST },
    { type: RELEASE_ARTIST, artist: FEATURE_ARTIST },
  ];

  let release = new Release();

  expect(release.getId()).not.toBe(ID);
  release.setId(ID);
  expect(release.getId()).toBe(ID);

  expect(release.getTitle()).not.toBe(TITLE);
  release.setTitle(TITLE);
  expect(release.getTitle()).toBe(TITLE);

  expect(release.getYear()).not.toBe(YEAR);
  release.setYear(YEAR);
  expect(release.getYear()).toBe(YEAR);

  expect(release.getTracks()).not.toEqual(expect.arrayContaining(TRACKS));
  release.setTracks(TRACKS);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS));

  expect(release.getArtistRelations()).not.toEqual(ARTIST_RELATIONS);
  release.setArtistRelations(ARTIST_RELATIONS);
  expect(release.getArtistRelations()).toEqual(ARTIST_RELATIONS);
});

test("toString() returns data as string", () => {
  const ID = "A";
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const ARTIST = new Artist("A", "artist_a");
  const FEATURE_ARTIST = new Artist("B", "artist_b");
  const ARTIST_RELATIONS = [
    { type: RELEASE_ARTIST, artist: ARTIST },
    { type: RELEASE_ARTIST, artist: FEATURE_ARTIST },
  ];

  let release = new Release(ID, TITLE, YEAR, TRACKS, ARTIST_RELATIONS);
  expect(release.toString()).toBe(
    "Release {title: release; year: 2020; trackCount: 1; artists: artist_a,artist_b; id: A}"
  );
});

test("addTrack adds a track to the release and sets the tracks release accordingly", () => {
  const TRACK = new Track("A");

  let release = new Release();

  expect(release.getTracks()).not.toContain(TRACK);
  expect(TRACK.getRelease()).not.toBe(release);
  release.addTrack(TRACK);
  expect(release.getTracks()).toContain(TRACK);
  expect(TRACK.getRelease()).toBe(release);
});

test("addTracks adds multiple track to the release and sets the track release accordingly", () => {
  const TRACK_A = new Track("A");
  const TRACK_B = new Track("B");

  let release = new Release();

  expect(release.getTracks()).not.toContain(TRACK_A);
  expect(release.getTracks()).not.toContain(TRACK_B);
  expect(TRACK_A.getRelease()).not.toBe(release);
  expect(TRACK_B.getRelease()).not.toBe(release);
  release.addTracks([TRACK_A, TRACK_B]);
  expect(release.getTracks()).toContain(TRACK_A);
  expect(release.getTracks()).toContain(TRACK_B);
  expect(TRACK_A.getRelease()).toBe(release);
  expect(TRACK_B.getRelease()).toBe(release);
});

test("addArtist adds an artist to the release and adds the release to the artists relations", () => {
  const ARTIST = new Artist("artist");

  const release = new Release("release");

  expect(release.getArtists(RELEASE_ARTIST)).not.toContain(ARTIST);
  expect(ARTIST.getReleases(RELEASE_ARTIST)).not.toContain(release);
  release.addArtist(ARTIST, RELEASE_ARTIST);
  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST);
  expect(ARTIST.getReleases(RELEASE_ARTIST)).toContain(release);
});

test("addArtists adds multiple artists to the release and adds the release to the artists accordingly", () => {
  const ARTIST_A = new Artist("artist_a");
  const ARTIST_B = new Artist("artist_b");

  const release = new Release("release");

  expect(release.getArtists(RELEASE_ARTIST)).not.toContain(ARTIST_A);
  expect(release.getArtists(RELEASE_ARTIST)).not.toContain(ARTIST_B);
  expect(ARTIST_A.getReleases(RELEASE_ARTIST)).not.toContain(release);
  expect(ARTIST_B.getReleases(RELEASE_ARTIST)).not.toContain(release);
  release.addArtists([ARTIST_A, ARTIST_B], RELEASE_ARTIST);
  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST_A);
  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST_B);
  expect(ARTIST_A.getReleases(RELEASE_ARTIST)).toContain(release);
  expect(ARTIST_B.getReleases(RELEASE_ARTIST)).toContain(release);
});

test("getArtists returns artists of a specific relation type", () => {
  const ARTIST = new Artist("A", "artist_a");
  const FEATURE_ARTIST = new Artist("B", "artist_b");
  const ARTIST_RELATIONS = [
    { type: RELEASE_ARTIST, artist: ARTIST },
    { type: RELEASE_GUEST, artist: FEATURE_ARTIST },
  ];

  const release = new Release("release", "title", 2021, [], ARTIST_RELATIONS);

  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST);
  expect(release.getArtists(RELEASE_GUEST)).toContain(FEATURE_ARTIST);
});

test("deleteTrack deletes a track from the release using its id", () => {
  const TRACK = new Track("A");

  let release = new Release();
  release.addTrack(TRACK);

  expect(release.getTracks()).toContain(TRACK);
  release.deleteTrack("A");
  expect(release.getTracks()).not.toContain(TRACK);
});

test("complement adds missing data from provided release", () => {
  const ID = "A";
  const TITLE = "release";
  const YEAR = 2020;
  const TRACKS = [new Track("A")];
  const ARTIST = new Artist("artist");
  const FEATURE_ARTIST = new Artist("feature");
  const ARTIST_RELATIONS = [
    { type: RELEASE_ARTIST, artist: ARTIST },
    { type: RELEASE_GUEST, artist: FEATURE_ARTIST },
  ];

  let release = new Release();
  let complementingRelease = new Release(
    ID,
    TITLE,
    YEAR,
    TRACKS,
    ARTIST_RELATIONS
  );

  expect(release.getId()).not.toBe(ID);
  expect(release.getTitle()).not.toBe(TITLE);
  expect(release.getYear()).not.toBe(YEAR);
  expect(release.getTracks()).not.toEqual(expect.arrayContaining(TRACKS));
  expect(release.getArtists(RELEASE_ARTIST)).not.toContain(ARTIST);
  expect(release.getArtists(RELEASE_GUEST)).not.toContain(FEATURE_ARTIST);

  release = release.complement(complementingRelease);
  expect(release.getId()).toBe(ID);
  expect(release.getTitle()).toBe(TITLE);
  expect(release.getYear()).toBe(YEAR);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS));
  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST);
  expect(release.getArtists(RELEASE_GUEST)).toContain(FEATURE_ARTIST);
});

test("complement doesnt overwrite existing data", () => {
  const ID_A = "A";
  const ID_B = "B";
  const TITLE_A = "releaseA";
  const TITLE_B = "releaseB";
  const YEAR_A = 2020;
  const YEAR_B = 2019;
  const TRACKS_A = [new Track("A")];
  const TRACKS_B = [new Track("B")];
  const ARTIST_A = new Artist("artist_a");
  const ARTIST_B = new Artist("artist_b");
  const FEATURE_ARTIST_A = new Artist("feature_a");
  const FEATURE_ARTIST_B = new Artist("feature_b");
  const ARTIST_RELATIONS_A = [
    { type: RELEASE_ARTIST, artist: ARTIST_A },
    { type: RELEASE_GUEST, artist: FEATURE_ARTIST_A },
  ];
  const ARTIST_RELATIONS_B = [
    { type: RELEASE_ARTIST, artist: ARTIST_B },
    { type: RELEASE_GUEST, artist: FEATURE_ARTIST_B },
  ];

  let release = new Release(
    ID_A,
    TITLE_A,
    YEAR_A,
    TRACKS_A,
    ARTIST_RELATIONS_A
  );
  let complementingRelease = new Release(
    ID_B,
    TITLE_B,
    YEAR_B,
    TRACKS_B,
    ARTIST_RELATIONS_B
  );

  expect(release.getId()).toBe(ID_A);
  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST_A);
  expect(release.getArtists(RELEASE_GUEST)).toContain(FEATURE_ARTIST_A);
  expect(release.getTitle()).toBe(TITLE_A);
  expect(release.getYear()).toBe(YEAR_A);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS_A));
  release = release.complement(complementingRelease);
  expect(release.getId()).toBe(ID_A);
  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST_A);
  expect(release.getArtists(RELEASE_ARTIST)).toContain(ARTIST_B);
  expect(
    release.getArtists(RELEASE_GUEST).map((artist) => artist.getId())
  ).toContain(FEATURE_ARTIST_A.getId());
  expect(
    release.getArtists(RELEASE_GUEST).map((artist) => artist.getId())
  ).toContain(FEATURE_ARTIST_B.getId());
  expect(release.getTitle()).toBe(TITLE_A);
  expect(release.getYear()).toBe(YEAR_A);
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS_A));
  expect(release.getTracks()).toEqual(expect.arrayContaining(TRACKS_B));
});
