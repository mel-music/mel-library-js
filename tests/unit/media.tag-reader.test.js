const TagReader = require("../../src/media/tag-reader");
const NodeJsFileSystem = require("../../modules/node-js-file-system");
const File = require("../../src/files/file");
const { MP3, FLAC } = File;
const Logger = require("../../src/utils/logger");
const { OFF } = Logger;

const { crc32 } = require("js-crc");
const path = require("path");
const fileSystem = new NodeJsFileSystem();

const TEST_FILES_DIR = path.resolve("./tests/test-files");
const MP3_FILE_PATH = path.join(TEST_FILES_DIR, "test.mp3");
const MP3_FILE = new File(MP3_FILE_PATH, MP3);
const FLAC_FILE_PATH = path.join(TEST_FILES_DIR, "test.flac");
const FLAC_FILE = new File(FLAC_FILE_PATH, FLAC);

Logger.setLogLevel(OFF);

describe("without configuration", () => {
  let tagReader;

  beforeEach(() => {
    tagReader = new TagReader();
  });

  afterEach(() => {
    tagReader = null;
  });

  it("throws error when trying to read tags", async () => {
    await expect(tagReader.readTags(MP3_FILE)).rejects.toThrow();
  });
});

describe("with configuration", () => {
  let tagReader;

  beforeEach(() => {
    tagReader = new TagReader();
    tagReader.setConfig({ fileSystem });
  });

  afterEach(() => {
    tagReader = null;
  });

  it("reads tags from mp3 file", async () => {
    if (!MP3_FILE.getStats()) {
      let stats = await fileSystem.stats(MP3_FILE_PATH);
      MP3_FILE.setStats(stats);
    }
    const tags = await tagReader.readTags(MP3_FILE);

    expect(tags).toHaveProperty("trackTitle");
    expect(tags.trackTitle).toBe("Test Title");
    expect(tags).toHaveProperty("artistName");
    expect(tags.artistName).toBe("Test Artist");
    expect(tags).toHaveProperty("albumArtistName");
    expect(tags.albumArtistName).toBe("Test Album Artist");
    expect(tags).toHaveProperty("albumTitle");
    expect(tags.albumTitle).toBe("Test Album");
    expect(tags).toHaveProperty("year");
    expect(tags.year).toBe("2020");
    expect(tags).toHaveProperty("trackNumber");
    expect(tags.trackNumber).toBe("1");
    expect(tags).toHaveProperty("discNumber");
    expect(tags.discNumber).toBe("1");
  });

  it("reads album cover from mp3 file", async () => {
    if (!MP3_FILE.getStats()) {
      let stats = await fileSystem.stats(MP3_FILE_PATH);
      MP3_FILE.setStats(stats);
    }
    const albumCover = await tagReader.readAlbumCover(MP3_FILE);

    expect(albumCover).toHaveProperty("format");
    expect(albumCover.format).toBe("image/jpeg");
    expect(albumCover).toHaveProperty("data");
    let crc = crc32(new Uint8Array(albumCover.data));
    expect(crc).toBe("4b76cf84");
  });

  it("reads tags from flac file", async () => {
    if (!FLAC_FILE.getStats()) {
      let stats = await fileSystem.stats(FLAC_FILE_PATH);
      FLAC_FILE.setStats(stats);
    }
    const tags = await tagReader.readTags(FLAC_FILE);

    expect(tags).toHaveProperty("trackTitle");
    expect(tags.trackTitle).toBe("Test Title");
    expect(tags).toHaveProperty("artistName");
    expect(tags.artistName).toBe("Test Artist");
    expect(tags).toHaveProperty("albumArtistName");
    expect(tags.albumArtistName).toBe("Test Album Artist");
    expect(tags).toHaveProperty("albumTitle");
    expect(tags.albumTitle).toBe("Test Album");
    expect(tags).toHaveProperty("year");
    expect(tags.year).toBe("2020");
    expect(tags).toHaveProperty("trackNumber");
    expect(tags.trackNumber).toBe("1");
    expect(tags).toHaveProperty("discNumber");
    expect(tags.discNumber).toBe("1");
  });

  it("reads album cover from flac file", async () => {
    if (!FLAC_FILE.getStats()) {
      let stats = await fileSystem.stats(FLAC_FILE_PATH);
      FLAC_FILE.setStats(stats);
    }
    const albumCover = await tagReader.readAlbumCover(FLAC_FILE);

    expect(albumCover).toHaveProperty("format");
    expect(albumCover.format).toBe("image/jpeg");
    expect(albumCover).toHaveProperty("data");
    let crc = crc32(new Uint8Array(albumCover.data));
    expect(crc).toBe("4b76cf84");
  });
});
