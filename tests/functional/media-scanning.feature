Feature: Scanning directories for media files
  The Mel Library shall scan a given directory for media files

  Scenario: Library scans a valid directory
    Given a valid directory path was provided
    And configuration is set
    When a complete scan is performed
    Then the scanned files can be returned

  Scenario: Adding source directories
    Given no directories were provided
    When a directory is added
    Then the directory can be returned

  Scenario: Meta data is extracted from music files
    Given a valid directory path was provided
    And configuration is set
    When a complete scan is performed
    Then the music files meta data can be returned
    And release cover can be returned

  Scenario: IDs of returned meta data entities are correct
    Given a valid directory path was provided
    And configuration is set
    When a complete scan is performed
    Then the artist, release and track objects have correct IDs

  Scenario: Library scans only MP3 when configured accordingly
    Given a valid directory path was provided
    And configuration is set to only scan MP3 files
    When a complete scan is performed
    Then the scanned MP3 files can be returned

  Scenario: Library scans only FLAC when configured accordingly
    Given a valid directory path was provided
    And configuration is set to only scan FLAC files
    When a complete scan is performed
    Then the scanned FLAC files can be returned

  Scenario: Multiple library scans don't change available files
    Given a valid directory path was provided
    And configuration is set
    When a complete scan is performed
    And a complete scan is performed
    And a complete scan is performed
    Then the scanned files can be returned

  Scenario: Multiple library scans don't change available metadata
    Given a valid directory path was provided
    And configuration is set
    When a complete scan is performed
    And a complete scan is performed
    And a complete scan is performed
    Then the music files meta data can be returned

  Scenario: Media scanning is sufficiently fast
    Given a valid directory path was provided
    And configuration is set
    And time is measured
    When a complete scan is performed 300x
    Then the measured time is sufficiently low
