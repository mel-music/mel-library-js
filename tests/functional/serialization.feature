Feature: Serialization of data objects
  Serialization of data objects allows the transformation of class instances 
  into persistable string representations and vice versa


  Scenario: The serializer utility is accesible via the utils object of the MelLibrary
    When reading the serializer utility from the utils object
    Then the returned object has all functions for serialization and deserialization


  Scenario: Serialization of artist instances
    Given a valid artist instance
    When serializing the artist instance
    Then deserializing the artist object yields an equal artist instance

  Scenario: Serialization of artist instance arrays
    Given a valid artist instance array
    When serializing the artist instance array
    Then deserializing the artist object array yields an equal artist instance array

  Scenario: Serialization of artist instance with nested complex instances
    Given a valid artist instance with nested complex instances
    When serializing the artist instance
    Then deserializing the artist object yields an equal artist instance


  Scenario: Serialization of release instances
    Given a valid release instance
    When serializing the release instance
    Then deserializing the release object yields an equal release instance

  Scenario: Serialization of release instance arrays
    Given a valid release instance array
    When serializing the release instance array
    Then deserializing the release object array yields an equal release instance array

  Scenario: Serialization of release instance with nested complex instances
    Given a valid release instance with nested complex instances
    When serializing the release instance
    Then deserializing the release object yields an equal release instance


  Scenario: Serialization of track instances
    Given a valid track instance
    When serializing the track instance
    Then deserializing the track object yields an equal track instance

  Scenario: Serialization of track instance arrays
    Given a valid track instance array
    When serializing the track instance array
    Then deserializing the track object array yields an equal track instance array

  Scenario: Serialization of track instance with nested complex instances
    Given a valid track instance with nested complex instances
    When serializing the track instance
    Then deserializing the track object yields an equal track instance
