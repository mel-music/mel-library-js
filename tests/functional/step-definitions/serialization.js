const assert = require("assert");
const { Given, When, Then } = require("@cucumber/cucumber");

const MelLibrary = require("../../../index.js");
const Artist = require("../../../src/media/artist");
const Release = require("../../../src/media/release");
const Track = require("../../../src/media/track");
const Serializer = MelLibrary.utils.Serializer;
const { constants } = MelLibrary;

const { RELEASE_ARTIST, RELEASE_GUEST, TRACK_ARTIST } = constants.relationTypes;

Given("a valid artist instance", function () {
  this.artist = new Artist("artist_id", "artist_name");
});

Given("a valid artist instance array", function () {
  this.artists = [
    new Artist("artist_id_a", "artist_name_a"),
    new Artist("artist_id_b", "artist_name_b"),
    new Artist("artist_id_c", "artist_name_c"),
    new Artist("artist_id_d", "artist_name_d"),
  ];
});

Given("a valid artist instance with nested complex instances", function () {
  this.artist = new Artist("artist_id", "artist_name", [
    { type: RELEASE_ARTIST, release: new Release("", null, "release_a") },
    { type: RELEASE_ARTIST, release: new Release("", null, "release_b") },
    {
      type: RELEASE_GUEST,
      release: new Release("", null, "feature_release_a"),
    },
    {
      type: RELEASE_GUEST,
      release: new Release("", null, "feature_release_b"),
    },
  ]);
});

Given("a valid release instance", function () {
  this.release = new Release("release_id", null, "release_title", 2021);
});

Given("a valid release instance array", function () {
  this.release = [
    new Release("release_id_a", null, "release_title_a", 2021),
    new Release("release_id_b", null, "release_title_b", 2021),
    new Release("release_id_c", null, "release_title_c", 2021),
    new Release("release_id_d", null, "release_title_d", 2021),
  ];
});

Given("a valid release instance with nested complex instances", function () {
  this.release = new Release(
    "release_id",
    "release_title",
    2021,
    [new Track("track_a"), new Track("track_b")],
    [
      { type: TRACK_ARTIST, artist: new Artist("artist_id") },
      { type: TRACK_ARTIST, artist: new Artist("feature_artist_a") },
      { type: TRACK_ARTIST, artist: new Artist("feature_artist_b") },
    ]
  );
});

Given("a valid track instance", function () {
  this.track = new Track("track_id", "track_title", 1, 2);
});

Given("a valid track instance array", function () {
  this.tracks = [
    new Track("track_id_a", "track_title_a", 1, 2),
    new Track("track_id_b", "track_title_b", 1, 2),
    new Track("track_id_c", "track_title_c", 1, 2),
    new Track("track_id_d", "track_title_d", 1, 2),
  ];
});

Given("a valid track instance with nested complex instances", function () {
  this.track = new Track(
    "track_id",
    "track_title",
    [new Artist("artist_a"), new Artist("artist_b")],
    new Release("release_id")
  );
});

When("reading the serializer utility from the utils object", function () {
  this.serializer = MelLibrary.utils.Serializer;
});

When("serializing the artist instance", function () {
  this.artistObject = Serializer.serializeArtist(this.artist);
});

When("serializing the artist instance array", function () {
  this.artistObjects = Serializer.serializeArtists(this.artists);
});

When("serializing the release instance", function () {
  this.releaseObject = Serializer.serializeRelease(this.release);
});

When("serializing the release instance array", function () {
  this.releaseObjects = Serializer.serializeReleases(this.releases);
});

When("serializing the track instance", function () {
  this.trackObject = Serializer.serializeTrack(this.track);
});

When("serializing the track instance array", function () {
  this.trackObjects = Serializer.serializeTracks(this.tracks);
});

Then(
  "the returned object has all functions for serialization and deserialization",
  function () {
    const properties = Object.keys(this.serializer);

    assert(
      properties.indexOf("serializeArtist") !== -1,
      "Serializer contains serializeArtist function"
    );
    assert(
      properties.indexOf("serializeArtists") !== -1,
      "Serializer contains serializeArtists function"
    );
    assert(
      properties.indexOf("deserializeArtist") !== -1,
      "Serializer contains deserializeArtist function"
    );
    assert(
      properties.indexOf("deserializeArtists") !== -1,
      "Serializer contains deserializeArtists function"
    );
    assert(
      properties.indexOf("serializeRelease") !== -1,
      "Serializer contains serializeRelease function"
    );
    assert(
      properties.indexOf("serializeReleases") !== -1,
      "Serializer contains serializeReleases function"
    );
    assert(
      properties.indexOf("deserializeRelease") !== -1,
      "Serializer contains deserializeRelease function"
    );
    assert(
      properties.indexOf("deserializeReleases") !== -1,
      "Serializer contains deserializeReleases function"
    );
    assert(
      properties.indexOf("serializeTrack") !== -1,
      "Serializer contains serializeTrack function"
    );
    assert(
      properties.indexOf("serializeTracks") !== -1,
      "Serializer contains serializeTracks function"
    );
    assert(
      properties.indexOf("deserializeTrack") !== -1,
      "Serializer contains deserializeTrack function"
    );
    assert(
      properties.indexOf("deserializeTracks") !== -1,
      "Serializer contains deserializeTracks function"
    );
  }
);

Then(
  "deserializing the artist object yields an equal artist instance",
  function () {
    const deserializedArtist = Serializer.deserializeArtist(this.artistObject);
    assert(
      this.artist.equals(deserializedArtist),
      "Original artist equal deserialized artist"
    );
  }
);

Then(
  "deserializing the artist object array yields an equal artist instance array",
  function () {
    const deserializedArtists = Serializer.deserializeArtists(
      this.artistObjects
    );
    assert(
      !deserializedArtists.some(
        (artistA) => !this.artists.find((artistB) => artistA.equals(artistB))
      ),
      "Deserialized artist object array equals original artist instance array"
    );
  }
);

Then(
  "deserializing the release object yields an equal release instance",
  function () {
    const deserializedRelease = Serializer.deserializeRelease(
      this.releaseObject
    );
    assert(
      this.release.equals(deserializedRelease),
      "Original release equals deserialized release"
    );
  }
);

Then(
  "deserializing the release object array yields an equal release instance array",
  function () {
    const deserializedReleases = Serializer.deserializeReleases(
      this.releaseObjects
    );
    assert(
      !deserializedReleases.some(
        (releaseA) =>
          !this.releases.find((releaseB) => releaseA.equals(releaseB))
      ),
      "Deserialized release object array equals original release instance array"
    );
  }
);

Then(
  "deserializing the track object yields an equal track instance",
  function () {
    const deserializedTrack = Serializer.deserializeTrack(this.trackObject);
    assert(
      this.track.equals(deserializedTrack),
      "Original track equals deserialized track"
    );
  }
);

Then(
  "deserializing the track object array yields an equal track instance array",
  function () {
    const deserializedTracks = Serializer.deserializeTracks(this.trackObjects);
    assert(
      !deserializedTracks.some(
        (trackA) => !this.tracks.find((trackB) => trackA.equals(trackB))
      ),
      "Deserialized track object array equals original track instance array"
    );
  }
);
