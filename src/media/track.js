function Track(id, title, number, discNumber, release, artistRelations) {
  let _id = id;
  let _title = title;
  let _number = number;
  let _discNumber = discNumber;
  let _release = release;
  let _artistRelations = artistRelations;
  if (!_artistRelations) _artistRelations = [];

  function addArtist(newArtist, relationType) {
    if (!relationType) relationType = ARTIST;
    const index = _artistRelations.findIndex(
      (relation) =>
        relation.type === relationType &&
        relation.artist.getId() === newArtist.getId()
    );
    if (index === -1) {
      _artistRelations.push({ type: relationType, artist: newArtist });
    }
  }

  function addArtists(newArtists, relationType) {
    newArtists.forEach((artist) => addArtist(artist, relationType));
    return instance;
  }

  function complement(track) {
    const complementedTrack = new Track(
      _id || track.getId(),
      _title || track.getTitle(),
      _number || track.getNumber(),
      _discNumber || track.getDiscNumber(),
      _release || track.getRelease(),
      _artistRelations
    );
    track
      .getArtistRelations()
      .forEach((relation) =>
        complementedTrack.addArtist(relation.artist, relation.type)
      );
    return complementedTrack;
  }

  function toString() {
    return (
      `Track {` +
      `title: ${_title}, ` +
      `artists: ${
        _artistRelations
          ? _artistRelations.map((relation) => relation.artist.getName())
          : []
      }, ` +
      `release: ${_release.getTitle()}, ` +
      `number: ${_number}, ` +
      `discNumber: ${_discNumber}, ` +
      `id: ${_id}}`
    );
  }

  function equals(track) {
    let equal = true;
    equal = equal && _id === track.getId();
    equal = equal && _title === track.getTitle();
    equal = equal && _number === track.getNumber();
    equal = equal && _discNumber === track.getDiscNumber();
    equal = equal && !_release === !track.getRelease();
    if (equal && _release) {
      equal = equal && _release.equals(track.getRelease());
    }
    equal =
      equal &&
      !track
        .getArtists()
        .some(
          (relationA) =>
            relationA &&
            !_artists.find(
              (relationB) =>
                relationA.type === relationB.type &&
                relationA.artist.equals(relationB.artist)
            )
        );
    return equal;
  }

  function getArtists(relationType) {
    if (!relationType) return [];
    return _artistRelations
      .filter((relation) => relation.type === relationType)
      .map((relation) => relation.artist);
  }

  function getTitle() {
    return _title;
  }

  function setTitle(value) {
    _title = value;
    return instance;
  }

  function getArtistRelations() {
    return _artistRelations;
  }

  function setArtistRelations(value) {
    _artistRelations = value;
    return instance;
  }

  function getRelease() {
    return _release;
  }

  function setRelease(value) {
    _release = value;
    return instance;
  }

  function getNumber() {
    return _number;
  }

  function setNumber(value) {
    _number = value;
    return instance;
  }

  function getDiscNumber() {
    return _discNumber;
  }

  function setDiscNumber(value) {
    _discNumber = value;
    return instance;
  }

  function getId() {
    return _id;
  }

  function setId(value) {
    _id = value;
    return instance;
  }

  let instance = {
    addArtist,
    addArtists,
    complement,
    toString,
    equals,
    getTitle,
    setTitle,
    getArtists,
    getRelease,
    setRelease,
    getNumber,
    setNumber,
    getDiscNumber,
    setDiscNumber,
    getId,
    setId,
    getArtistRelations,
    setArtistRelations,
  };

  return instance;
}

module.exports = Track;
