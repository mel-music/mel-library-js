function Artist(id, name, releaseRelations) {
  let _id = id;
  let _name = name;
  let _releaseRelations = releaseRelations;
  if (!_releaseRelations) _releaseRelations = [];

  function addRelease(newRelease, relationType) {
    newRelease.addArtist(instance, relationType);
    let relation = _releaseRelations.find(
      (relation) =>
        relation.type === relationType &&
        relation.release.getId() === newRelease.getId()
    );
    if (!relation) {
      _releaseRelations.push({ type: relationType, release: newRelease });
    } else {
      relation.release.addTracks(newRelease.getTracks());
    }
  }

  function addReleases(releases, relationType) {
    releases.forEach((release) => addRelease(release, relationType));
  }

  function complement(artist) {
    const complementedArtist = new Artist(
      _id || artist.getId(),
      _name || artist.getName(),
      _releaseRelations
    );
    artist
      .getReleaseRelations()
      .forEach((relation) =>
        complementedArtist.addRelease(relation.release, relation.type)
      );
    return complementedArtist;
  }

  function toString() {
    return (
      `Artist {` +
      `name: ${_name}, ` +
      `releases: ${_releaseRelations.map((relation) =>
        relation.release.getTitle()
      )}, ` +
      `id: ${_id}}`
    );
  }

  function equals(artist) {
    if (!artist) return false;
    let equal = true;
    equal = equal && _id === artist.getId();
    equal = equal && _name === artist.getName();
    equal = equal && _releaseRelations.length === artist.getReleaseRelations().length;
    equal =
      equal &&
      !artist
        .getReleaseRelations()
        .some(
          (relationA) =>
            relationA &&
            !_releaseRelations.find(
              (relationB) =>
                relationA.type === relationB.type &&
                relationA.release.equals(relationB.release)
            )
        );
    return equal;
  }

  function getReleases(relationType) {
    if (!relationType) return [];
    return _releaseRelations
      .filter((relation) => relation.type === relationType)
      .map((relation) => relation.release);
  }

  function getName() {
    return _name;
  }

  function setName(value) {
    _name = value;
    return instance;
  }

  function getReleaseRelations() {
    return _releaseRelations;
  }

  function setReleaseRelations(value) {
    _releaseRelations = value;
    return instance;
  }

  function getId() {
    return _id;
  }

  function setId(value) {
    _id = value;
    return instance;
  }

  let instance = {
    addRelease,
    addReleases,
    complement,
    toString,
    equals,
    getName,
    setName,
    getReleases,
    getId,
    setId,
    getReleaseRelations,
    setReleaseRelations,
  };

  return instance;
}

module.exports = Artist;
