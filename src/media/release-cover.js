function ReleaseCover(releaseId, buffer, type) {
  let _releaseId = releaseId;
  let _buffer = buffer;
  let _type = type;

  let instance;

  function getReleaseId() {
    return _releaseId;
  }

  function setReleaseId(releaseId) {
    _releaseId = releaseId;
  }

  function getBuffer() {
    return _buffer;
  }

  function setBuffer(buffer) {
    _buffer = buffer;
  }

  function getType() {
    return _type;
  }

  function setType(type) {
    _type = type;
  }

  instance = {
    getReleaseId,
    setReleaseId,
    getBuffer,
    setBuffer,
    getType,
    setType,
  }

  return instance;
}

module.exports = ReleaseCover;
