function Release(id, title, year, tracks, artistRelations) {
  let _id = id;
  let _title = title;
  let _year = year;
  let _tracks = tracks;
  if (!_tracks) _tracks = [];
  let _artistRelations = artistRelations;
  if (!_artistRelations) _artistRelations = [];

  function addTrack(newTrack) {
    newTrack.setRelease(instance);
    if (
      _tracks.findIndex((track) => track.getId() === newTrack.getId()) === -1
    ) {
      _tracks.push(newTrack);
    }
  }

  function addTracks(tracks) {
    tracks.forEach((track) => addTrack(track));
  }

  function addArtist(newArtist, relationType) {
    if (!relationType) relationType = RELEASE;
    if (
      _artistRelations.findIndex(
        ({ type, artist }) =>
          type === relationType && artist.getId() === newArtist.getId()
      ) === -1
    ) {
      _artistRelations.push({ type: relationType, artist: newArtist });
      newArtist.addRelease(instance, relationType);
    }
  }

  function addArtists(artists, relationType) {
    artists.forEach((artist) => addArtist(artist, relationType));
  }

  function deleteTrack(trackId) {
    const index = _tracks.findIndex((track) => track.getId() === trackId);
    _tracks.splice(index, 1);
  }

  function complement(release) {
    const complementedRelease = new Release(
      _id || release.getId(),
      _title || release.getTitle(),
      _year || release.getYear(),
      _tracks,
      _artistRelations
    );
    release
      .getArtistRelations()
      .forEach((relation) =>
        complementedRelease.addArtist(relation.artist, relation.type)
      );
    complementedRelease.addTracks(release.getTracks());
    return complementedRelease;
  }

  function toString() {
    return (
      `Release {` +
      `title: ${_title}; ` +
      `year: ${_year}; ` +
      `trackCount: ${_tracks.length}; ` +
      `artists: ${_artistRelations.map((relation) =>
        relation.artist.getName()
      )}; ` +
      `id: ${_id}}`
    );
  }

  function equals(release) {
    let equal = true;
    equal = equal && _id === release.getId();
    equal = equal && _title === release.getTitle();
    equal = equal && _year === release.getYear();
    equal =
      equal &&
      !release
        .getArtistRelations()
        .some(
          (relationA) =>
            relationA &&
            !_artistRelations.find(
              (relationB) =>
                relationA.type === relationB.type &&
                relationA.artist.equals(relationB.artist)
            )
        );
    equal =
      equal &&
      !release
        .getTracks()
        .some(
          (trackA) => trackA && !_tracks.find((trackB) => trackA.equals(trackB))
        );
    return equal;
  }

  function getArtists(relationType) {
    if (!relationType) return [];
    return _artistRelations
      .filter((relation) => relation.type === relationType)
      .map((relation) => relation.artist);
  }

  function setArtistRelations(artistRelations) {
    _artistRelations = artistRelations;
    return instance;
  }

  function getArtistRelations() {
    return _artistRelations;
  }

  function getTitle() {
    return _title;
  }

  function setTitle(value) {
    _title = value;
    return instance;
  }

  function getYear() {
    return _year;
  }

  function setYear(value) {
    _year = value;
    return instance;
  }

  function getTracks() {
    return _tracks;
  }

  function setTracks(value) {
    _tracks = value;
    return instance;
  }

  function getId() {
    return _id;
  }

  function setId(value) {
    _id = value;
    return instance;
  }

  let instance = {
    addTrack,
    addTracks,
    addArtist,
    addArtists,
    deleteTrack,
    complement,
    toString,
    equals,
    getArtists,
    getTitle,
    setTitle,
    getYear,
    setYear,
    getTracks,
    setTracks,
    getId,
    setId,
    getArtistRelations,
    setArtistRelations,
  };

  return instance;
}

module.exports = Release;
