const { MediaFileReader } = require("jsmediatags/dist/jsmediatags.js");

function FileReader(file) {
  let _file = file;
  let _data = [];
  let _fileSystem = FileReader.fileSystem;

  function init(callbacks) {
    callbacks.onSuccess();
  }

  function getSize() {
    return _file.getStats().size;
  }

  function loadRange(range, callbacks) {
    (async () => {
      try {
        range = { start: range[0], end: range[1] };
        const data = _data.find(
          ({ range: loadedRange }) =>
            loadedRange.start <= range.start && loadedRange.end >= range.end
        );
        if (data) {
          callbacks.onSuccess();
          return;
        }
        const filePath = _file.getPath();
        const dataView = await _fileSystem.read(filePath, range);
        _data.push({ range, dataView });
        // console.log(this._data);
        callbacks.onSuccess();
      } catch (e) {
        callbacks.onError(e);
      }
    })();
  }

  function getBytesLoaded(param) {
    console.log("getBytesLoaded NOT IMPLEMENTED!");
  }

  function getByteAt(offset) {
    const data = _data.find(
      ({ range }) => range.start <= offset && range.end >= offset
    );
    if (!data) {
      throw Error(`Offset ${offset} hasn't been loaded yet.`);
    }
    const dataView = data.dataView;
    offset = offset - data.range.start;
    return dataView.getUint8(offset);
  }

  let _mediaFileReader = new MediaFileReader();
  _mediaFileReader.init = init;
  _mediaFileReader.getSize = getSize;
  _mediaFileReader.loadRange = loadRange;
  _mediaFileReader.getBytesLoaded = getBytesLoaded;
  _mediaFileReader.getByteAt = getByteAt;

  return _mediaFileReader;
}

FileReader.canReadFile = function (file) {
  let canRead = true;
  canRead = canRead && file.getStats;
  canRead = canRead && file.getPath;
  return canRead;
};

module.exports = FileReader;
