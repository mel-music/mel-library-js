const EventEmitter = require("./event-emitter");
const Logger = require("../utils/logger");

const logger = Logger.createNamespace("JobQueue");

const QUEUE_EMPTY_EVENT = "queue_empty_event";
const DEFAULT_LIMIT = 1;
const DEFAULT_GROUP = "default";

function JobQueue({ limits = {} } = {}) {
  let _jobs = {};
  let _processingJobs = false;
  let _limits = limits;
  let _runningJobs = {};
  let _eventEmitter = new EventEmitter();
  let _currentGroup;

  async function _processJobs() {
    if (_processingJobs) {
      return;
    }
    _processingJobs = true;
    logger.debug("Processing jobs");

    while (totalJobs() > 0) {
      const group = _nextJobsGroup();
      if (!_currentGroup || _runningJobsCount(_currentGroup) === 0) {
        _currentGroup = group;
      }
      if (group !== _currentGroup) break;
      if (_runningJobsCount(group) >= _groupLimit(group)) break;
      const job = _nextJob();
      _runJob(job);
    }
    _processingJobs = false;
  }

  function queueJob(fn, options = {}) {
    logger.debug("Queueing job");
    const priority = options.priority || 50;
    const group = options.group || DEFAULT_GROUP;
    _jobs[priority] = _jobs[priority] || [];
    const promise = new Promise((resolve, reject) =>
      _jobs[priority].push({ fn, resolve, reject, group })
    );
    _processJobs();
    return promise;
  }

  function totalJobs() {
    const jobCount = Object.keys(_jobs).reduce(
      (sum, priority) => sum + _jobs[priority].length,
      0
    );
    return jobCount;
  }

  async function _runJob(job) {
    logger.debug("Running job");
    const { group } = job;
    _runningJobs[group] = _runningJobs[group] || [];
    _runningJobs[group].push(job);
    try {
      job.resolve(await job.fn());
    } catch (error) {
      job.reject(error);
    }
    const index = _runningJobs[group].indexOf(job);
    _runningJobs[group].splice(index, 1);
    if (_runningJobs[group].length === 0) delete _runningJobs[group];

    logger.debug("Job finished");
    if (totalJobs() === 0 && _runningJobsCount(group) === 0) {
      _eventEmitter.dispatch(QUEUE_EMPTY_EVENT);
    } else {
      _processJobs();
    }
  }

  function _nextJob() {
    const nextPriority = _nextPriority();
    return _jobs[nextPriority].shift();
  }

  function _nextJobsGroup() {
    const nextPriority = _nextPriority();
    if (!nextPriority) return null;
    return _jobs[nextPriority][0].group;
  }

  function _nextPriority() {
    return Object.keys(_jobs)
      .filter((priority) => _jobs[priority].length > 0)
      .sort((priorityA, priorityB) => priorityB - priorityA)[0];
  }

  function _runningJobsCount(group) {
    if (!_runningJobs[group]) return 0;
    return _runningJobs[group].length;
  }

  function _groupLimit(group) {
    if (!_limits[group]) return DEFAULT_LIMIT;
    return _limits[group];
  }

  function onQueueEmpty(callback) {
    _eventEmitter.on(QUEUE_EMPTY_EVENT, callback);
  }

  return {
    queueJob,
    totalJobs,
    onQueueEmpty,
  };
}

module.exports = JobQueue;
