const Artist = require("../media/artist");
const Release = require("../media/release");
const Track = require("../media/track");

function Serializer() {
  function serializeArtist(artist) {
    if (!artist) return null;
    const artistObject = {
      id: artist.getId(),
      name: artist.getName(),
      release_relations: artist.getReleaseRelations().map((relation) => ({
        type: relation.type,
        release: serializeRelease(relation.release),
      })),
    };
    return artistObject;
  }

  function serializeArtists(artists) {
    if (!artists || !(artists instanceof Array)) return [];
    const artistObjects = artists.map((artist) => serializeArtist(artist));
    return artistObjects;
  }

  function deserializeArtist(artistObject) {
    if (!artistObject) return null;
    let { id, name, release_relations: releaseRelations } = artistObject;
    if (releaseRelations) {
      releaseRelations = releaseRelations.map((relation) => ({
        type: relation.type,
        release: deserializeRelease(relation.release),
      }));
    } else {
      releaseRelations = [];
    }
    const artist = new Artist(id, name, releaseRelations);
    return artist;
  }

  function deserializeArtists(artistObjects) {
    if (!artistObjects || !(artistObjects instanceof Array)) return [];
    const artists = artistObjects.map((artistObject) =>
      deserializeArtist(artistObject)
    );
    return artists;
  }

  function serializeRelease(release) {
    if (!release) return null;
    const releaseObject = {
      id: release.getId(),
      title: release.getTitle(),
      year: release.getYear(),
      tracks: serializeTracks(release.getTracks()),
      artist_relations: release.getArtistRelations().map((relation) => ({
        type: relation.type,
        artist: serializeArtist(relation.artist),
      })),
    };
    return releaseObject;
  }

  function serializeReleases(releases) {
    if (!releases || !(releases instanceof Array)) return [];
    const releaseObjects = releases.map((release) => serializeRelease(release));
    return releaseObjects;
  }

  function deserializeRelease(releaseObject) {
    if (!releaseObject) return null;
    let {
      id,
      title,
      year,
      tracks: trackObjects,
      artist_relations: artistRelations,
    } = releaseObject;
    if (artistRelations) {
      artistRelations = artistRelations.map((relation) => ({
        type: relation.type,
        artist: deserializeArtist(relation.artist),
      }));
    }
    const tracks = deserializeTracks(trackObjects);
    const release = new Release(id, title, year, tracks, artistRelations);
    return release;
  }

  function deserializeReleases(releaseObjects) {
    if (!releaseObjects || !(releaseObjects instanceof Array)) return [];
    const releases = releaseObjects.map((releaseObject) =>
      deserializeRelease(releaseObject)
    );
    return releases;
  }

  function serializeTrack(track) {
    if (!track) return null;
    const trackObject = {
      id: track.getId(),
      title: track.getTitle(),
      number: track.getNumber(),
      disc_number: track.getDiscNumber(),
      release: serializeRelease(track.getRelease()),
      artist_relations: track.getArtistRelations().map((relation) => ({
        type: relation.type,
        artist: serializeArtist(relation.artist),
      })),
    };
    return trackObject;
  }

  function serializeTracks(tracks) {
    if (!tracks || !(tracks instanceof Array)) return [];
    const trackObjects = tracks.map((track) => serializeTrack(track));
    return trackObjects;
  }

  function deserializeTrack(trackObject) {
    if (!trackObject) return null;
    let {
      id,
      title,
      number,
      disc_number,
      release: releaseObject,
      artist_relations: artistRelations,
    } = trackObject;
    const release = deserializeRelease(releaseObject);
    if (artistRelations) {
      artistRelations = artistRelations.map((relation) => ({
        type: relation.type,
        artist: deserializeArtist(relation.artist),
      }));
    }
    const track = new Track(
      id,
      title,
      number,
      disc_number,
      release,
      artistRelations
    );
    return track;
  }

  function deserializeTracks(trackObjects) {
    if (!trackObjects || !(trackObjects instanceof Array)) return [];
    const tracks = trackObjects.map((trackObject) =>
      deserializeTrack(trackObject)
    );
    return tracks;
  }

  return {
    serializeArtist,
    serializeArtists,
    deserializeArtist,
    deserializeArtists,
    serializeRelease,
    serializeReleases,
    deserializeRelease,
    deserializeReleases,
    serializeTrack,
    serializeTracks,
    deserializeTrack,
    deserializeTracks,
  };
}

module.exports = new Serializer();
