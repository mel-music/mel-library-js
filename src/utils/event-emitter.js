function EventEmitter() {
  let _listeners = {};

  function on(name, callback) {
    if (!_listeners[name]) {
      _listeners[name] = [];
    }
    _listeners[name].push(callback);
  }

  function off(listener) {
    if (!listener) {
      return;
    }

    for (let name in _listeners) {
      const index = _listeners[name].indexOf(listener);
      _listeners[name].splice(index, 1);
      break;
    }
  }

  function dispatch(name, data) {
    if (!_listeners[name]) {
      return;
    }
    _listeners[name].forEach((listener) => listener(data));
  }

  function clearListeners() {
    _listeners = [];
  }

  return {
    on,
    off,
    dispatch,
    clearListeners,
  };
}

module.exports = EventEmitter;
