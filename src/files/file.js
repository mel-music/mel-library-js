const MP3 = "mp3";
const FLAC = "flac";

function File(path, type, buffer = null, stats = null, track) {
  let _path = path;
  let _type = type;
  let _buffer = buffer;
  let _stats = stats;
  let _track = track;

  function deleteBuffer() {
    _buffer = null;
  }

  function getPath() {
    return _path;
  }

  function setPath(value) {
    _path = value;
  }

  function getType() {
    return _type;
  }

  function setType(value) {
    _type = value;
  }

  function getBuffer() {
    return _buffer;
  }

  function setBuffer(value) {
    _buffer = value;
  }

  function getStats() {
    return _stats;
  }

  function setStats(value) {
    _stats = value;
  }

  function getTrack() {
    return _track;
  }

  function setTrack(value) {
    _track = value;
  }

  return {
    deleteBuffer,
    getPath,
    setPath,
    getType,
    setType,
    getBuffer,
    setBuffer,
    getStats,
    setStats,
    getTrack,
    setTrack,
  };
}

File.MP3 = MP3;
File.FLAC = FLAC;

module.exports = File;
