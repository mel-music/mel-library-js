const Constants = {
  relationTypes: {
    /**
     * Artist published the release.
     * @constant
     * @type {string}
     */
    RELEASE_ARTIST: "release_artist",

    /**
     * Artist has guest appearance on release.
     * @constant
     * @type {string}
     */
    RELEASE_GUEST: "release_guest",

    /**
     * Artist performs on track.
     * @constant
     * @type {string}
     */
    TRACK_ARTIST: "track_artist",
  },
};

module.exports = Constants;
